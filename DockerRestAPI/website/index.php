<html>
    <head>
        <title>Home</title>
    </head>

    <body style="font-family:courier;">
        <h4>Select a button to be redirected to that page:</h4>
        <button id="all">List all times</button>
        <button id="open">List only open times</button>
        <button id="close">List only close times</button>
    </body>
</html>

<script type="text/javascript">
    document.getElementById("all").onclick = function () {
    window.location = "http://0.0.0.0:5001/listAll.php";
    };
    document.getElementById("open").onclick = function () {
    window.location = "http://0.0.0.0:5001/listOpenOnly.php";
    };
    document.getElementById("close").onclick = function () {
    window.location = "http://0.0.0.0:5001/listCloseOnly.php";
    };
</script>