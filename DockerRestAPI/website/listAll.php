<html>
    <head>
        <title>All Times</title>
    </head>

    <body style="font-family:courier;">
        <p><pre><b>   <u>Distance</u>      <u>Open</u>            <u>Close</u></b></pre></p>
        <ul>
            <?php
            if (isset($_GET['top'])) {
                $i = $_GET['top'];
            } else {
                $i = 20;
            }
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
            $cp = $obj->checkpoints;
            foreach ($obj as $x){
                if ($i == 0){
                    break;
                }
                $n = $x->km;
                $o = $x->open_time;
                $c = $x->close_time;
                echo "<li>$n km - $o - $c</li>";
                $i--;
            }
            ?>
        </ul>
    </body>
</html>
