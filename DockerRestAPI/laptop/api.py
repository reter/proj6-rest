import flask
import arrow  
import acp_times  
import config
import logging
import os
import time
import json
import csv
from flask import Flask, redirect, url_for, request, render_template, abort, jsonify, make_response
import pymongo
from pymongo import MongoClient
from flask_restful import Resource, Api, reqparse, marshal_with, fields

# Globals
app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
FIELDS = {
    "checkpoints":[]
}



class listAll(Resource):
    listAllFields = {
        'km': fields.Integer,
        'open_time': fields.String,
        'close_time': fields.String
    }
    @marshal_with(listAllFields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top', type=int)
        arg = parser.parse_args()
        if arg.get('top', 0) is None:
            arg = 20
        else:
            arg = arg['top']
        _items = db.tododb.find()
        items = []
        for item in _items:
            if arg is 0:
                break
            items.append(item)
            arg -= 1
        return items
api.add_resource(listAll, '/listAll', '/listAll/json', '/listAll/', '/listAll/json/')

class listOpenOnly(Resource):
    listOpenFields = {
        'km': fields.Integer,
        'open_time': fields.String
    }
    @marshal_with(listOpenFields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top', type=int)
        arg = parser.parse_args()
        if arg.get('top', 0) is None:
            arg = 20
        else:
            arg = arg['top']
        _items = db.tododb.find()
        items = []
        for item in _items:
            if arg is 0:
                break
            items.append(item)
            arg -= 1
        return items
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json', '/listOpenOnly/', '/listOpenOnly/json/')

class listCloseOnly(Resource):
    listCloseFields = {
        'km': fields.Integer,
        'close_time': fields.String
    }
    @marshal_with(listCloseFields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top', type=int)
        arg = parser.parse_args()
        if arg.get('top', 0) is None:
            arg = 20
        else:
            arg = arg['top']
        _items = db.tododb.find()
        items = []
        for item in _items:
            if arg is 0:
                break
            items.append(item)
            arg -= 1
        return items
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json', '/listCloseOnly/', '/listCloseOnly/json/')

class listAllCsv(Resource):
    def get(self):
        allJson = listAll.get(listAll)
        return csvify(allJson)
api.add_resource(listAllCsv, '/listAll/csv', '/listAll/csv/')

class listOpenOnlyCsv(Resource):
    def get(self):
        openJson = listOpenOnly.get(listOpenOnly)
        return csvify(openJson)
api.add_resource(listOpenOnlyCsv, '/listOpenOnly/csv', '/listOpenOnly/csv/')

class listCloseOnlyCsv(Resource):
    def get(self):
        closeJson = listCloseOnly.get(listCloseOnly)
        return csvify(closeJson)
api.add_resource(listCloseOnlyCsv, '/listCloseOnly/csv', '/listCloseOnly/csv/')

def csvify(temp):
    all_csv = open('toList.csv', 'w')
    csvwriter = csv.writer(all_csv)
    count = 0
    for thing in temp:
        if count == 0:
            header = thing.keys()
            csvwriter.writerow(header)
            count += 1
        csvwriter.writerow(thing.values())
    all_csv.close()
    all_csv = open('toList.csv', 'r')
    return flask.Response(all_csv, mimetype="text/csv", headers={"Content-disposition": "attachment; filename=toList.csv"})



@app.route('/submit', methods=['POST'])
def submit():
    data = request.form                             

    miles = []                                      
    km = []                                         
    location = []                                   
    open_times = []                                 
    close_times = []                                
    size = 0                                        

    for key in data.keys():                         
        for value in data.getlist(key):             
            if key == 'miles' and value != '':      
                miles.append(value)                 
                size += 1                           
            elif key == 'km' and value != '':       
                km.append(value)
            elif key == 'open' and value != '':
                open_times.append(value)
            elif key == 'close' and value != '':
                close_times.append(value)
            elif key == 'distance':
                dist = value                        

    i = 0
    for value in data.getlist('location'):          
        location.append(value)                      
        i += 1
        if i == size:                               
            break

    db.tododb.remove({})                            
    for x in range(size):
        item_doc = {                                
           'distance': dist,                        
           'miles': miles[x],                       
           'km': km[x],
           'location': location[x],
           'open_time': open_times[x],
           'close_time': close_times[x]
        }
        db.tododb.insert_one(item_doc)              
        dist = ""                                   
    return render_template('calc.html', reload=1)  

@app.route('/done')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)

@app.route("/")
@app.route("/index")
def index():
    doc_count = db.tododb.count()
    if doc_count is not 0:
        return flask.render_template('calc.html', reload=1)
    return flask.render_template('calc.html', reload=0)

@app.errorhandler(404)
def page_not_found(error):
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

# AJAX request handlers (returns json)
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', -1, type=float)                         
    begin_time = request.args.get('begin_time')                         
    begin_date = request.args.get('begin_date')                         
    distance = request.args.get('distance')                             

    if km == -1:                                                        
        result = {"error": 2}                                           
        return flask.jsonify(result=result)

    distance = float(distance)                                          
    if km > distance * 1.2:                                             
        result = {"error": 1}                                           
        return flask.jsonify(result=result)                             

    time = begin_date + " " + begin_time
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)
    long_enough = (km >= distance)

    result = {"open": open_time, "close": close_time, "error": 0, "enough": long_enough}
    return flask.jsonify(result=result)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
