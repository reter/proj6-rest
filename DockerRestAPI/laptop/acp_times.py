import arrow

open_dict = {200:34, 400:32, 600:30, 1000:28, 1300:26}

close_dict = {600:15, 1000:11.428, 1300:13.333}



def open_time(control_dist_km, brevet_dist_km, brevet_start_time):


    alt_distance = control_dist_km


    vo={'cot':0,'ld':0 }

    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km

    while control_dist_km > 0:
        for distance in open_dict:
            if control_dist_km > distance:
                vo['cot'] += (distance-vo['ld'])/open_dict[distance]
                alt_distance -= (distance-vo['ld'])
                ld = distance
            else:
                vo['cot'] += alt_distance/open_dict[distance]
                control_dist_km = 0
                break



    td = {'h': int(vo['cot']), 'm': int((vo['cot'] * 60) % 60), 's': int((vo['cot'] * 3600) % 60)}


    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    date_time = date_time.shift(hours=+td['h'], minutes=+td['m'] )
    date_time = date_time.replace(tzinfo='US/Pacific')


    if td['s']  >= 30:
        date_time = date_time.shift(minutes=+1)
    return date_time.isoformat()






def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    sp_dict = {1000:4500, 600:2400, 400:1620, 300:1200, 200:810}
    alt_distance = control_dist_km
    tp = control_dist_km



    vo = {'cct': 0, 'ld': 0}

    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km

    while control_dist_km > 0:
        for distance in close_dict:
            if control_dist_km > distance:
                vo['cct'] += (distance-vo['ld'])/close_dict[distance]
                alt_distance -= (distance-vo['ld'])
                ld = distance
            else:
                vo['cct'] += alt_distance/close_dict[distance]
                control_dist_km = 0
                break

    td = {'h': int(vo['cct']), 'm': int((vo['cct'] * 60) % 60), 's': int((vo['cct'] * 3600) % 60)}
    if tp == 0:
        td['h'] = 1
    if tp == brevet_dist_km:
        td['h'] = td['s'] = 0
        td['m'] = sp_dict[brevet_dist_km]
    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    date_time = date_time.shift(hours=+td['h'], minutes=+td['m'])
    date_time = date_time.replace(tzinfo='US/Pacific')
    if td['s'] >= 30:
        date_time = date_time.shift(minutes=+1)
    return date_time.isoformat()
